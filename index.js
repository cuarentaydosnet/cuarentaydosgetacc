const requestJson = require('request-json');

exports.handler = function(event,context,callback){
    const user = event.requestContext.authorizer;
    const apikey = event.stageVariables.mLabAPIKey;
    const db = event.stageVariables.baseMLabURL;
    console.log(event)
    
    var query = 'account?'
    
    //querystring
    var showid = (event.queryStringParameters && event.queryStringParameters.showid == "true") ? 'f={ "movimientos" : 0}&' : 'f={ "_id" : 0 , "movimientos" : 0}&' ;
    
    var mlabquery = {}
    if(event.queryStringParameters && event.queryStringParameters.owner) mlabquery.owner=event.queryStringParameters.owner
    if(event.queryStringParameters && event.queryStringParameters.tipo) mlabquery.tipo=Number(event.queryStringParameters.tipo)
    if(event.resource == '/usuarios/{userid}/cuentas') mlabquery.owner=event.pathParameters.userid
    if(event.pathParameters && event.pathParameters.accountid) mlabquery._id = event.pathParameters.accountid

    query = query + showid + 'q=' + JSON.stringify(mlabquery) + '&' + apikey
  
    console.log(query)
    
    var httpClient = requestJson.createClient(db);
    httpClient.get(query,
        function(err,resMLab,body){
            if(err){
                var response = {
                   "statusCode" : 501,
                   "headers" : {"Access-Control-Allow-Origin": "*"},
                    "body": JSON.stringify(err),
                }
            } else {
                    console.log(body)
                // body es un array de cuentas
                    if (user.tipo == 0){
                        var miscuentas =[];
                        for (var cuenta of body){
                            if( cuenta.owner == user._id) miscuentas.push(cuenta)
                        }
                        var response = { 
                        "statusCode": 200,
                        "headers" : {
                            "Access-Control-Allow-Origin": "*"
                        },                        
                        "body": JSON.stringify(miscuentas),
                        "isBase64Encoded": false
                        };
                        console.log(response)
                    } else {
                        var response = { 
                        "statusCode": 200,
                        "headers" : {
                            "Access-Control-Allow-Origin": "*"
                        },                        
                        "body": JSON.stringify(body),
                        "isBase64Encoded": false
                        };
                    }
                }
            callback(null,response); 
        })
};

